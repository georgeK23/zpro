# GNUplot setup  
1) Nejprve si stahnete GNUplot pro Windows (64bit) [zde](https://sourceforge.net/projects/gnuplot/files/gnuplot/5.2.7/gp527-win64-mingw.exe/download). 

2) Po stazeni instalujte klasicky, ze se odklika **Next**. Na plose se Vam vytvori zastupce GNUplot. Pokud se tak nestane, tak GNUplot se defaultne nainstaluje do **Program Files/gnuplot**. No a zde je slozka **bin**, v ktere je **gnuplot.exe**, takze si udelejte zastupce na plose. Takto mate nainstalovany GNUplot.

DALE CTETE POKUD CHCETE VYTVORIT HISTOGRAM NA FYS

3) Po spusteni se Vam ukaze prikazova radka a ted zacina ta sranda :) Ale my to obejdeme tak, ze si pullnete z GitLabu klasicky nejnovejsi verzi - jenom prosim, az budete pullovat, tak zaskrtnete **Merge** a **Shelve** viz. obrazek na Slacku ;)

4) Pridal jsem do **lecture_10/graph** nove funkce, kde **create_file()** vam do souboru **data.txt** nasype hodnoty z toho naseho sinu. Ale to je jenom na hrani - je tam totiz funkce **create_my_file()** a tu kdyz spustite v main, tak vas vyzve k zadavani hodnot, ktere tak muzete rucne zadat a ulozi Vam je do souboru **my_data.txt**. Samozrejme, ze ty data muzete rucne napsat do toho souboru a nemusite ani spoustet tu funkci ;) 

5) Pro vykresleni GNUplotem staci otevrit okno **Terminal** v CLion a tam napiste **gnuplot.exe histogram.txt**. Pokud by toto neslo (nemam po ruce zadny Windows, tak nemohu vyskouset), tak delejte nasledujici: 

Spuste prikazovou radku na Windows a dostante se do slozky **zpro** tak, ze napisete **cd PATH_TO_FOLDER**, kde **PATH_TO_FOLDER** je cesta ke slozce **zpro** (tuto cestu ziskate tak, ze pravym tlacitkem kliknete na slozku zpro v CLion a date **Copy Path**). No a kdyz jste jiz v te slozce (samozrejme myslim, v ramci prikazove radky) tak zadejte **gnuplot.exe histogram.txt**.


