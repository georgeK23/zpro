cmake_minimum_required(VERSION 3.14)
project(zpro)

set(CMAKE_CXX_STANDARD 14)

add_executable(zpro main.cpp)